
Vin227-HomeControl is a personal android GUI application for controlling IoT devices connected to a LAN. At the moment it works via UDP strings but in future it is planned to feature a more suitable protocol. 
The application is not meant to be used in other enviroments and is 100% personal project.
## Current functionality support
- Allows scanning for the devices in local network (Done via UDP broadcast. UPNP might be too intensive for Arduino uno.)
- Automatically detect if device comes live when app is open. (Actively listen when foreground.)
- Allows controlling different hardcoded functinalities listed below:
### Device functionalities
- RGB Leds color and brightness (Arduino server)
- Single color lighting toggle (Arduino server)
- Speaker amplifier power & input selection (Requires physical modifications to the device) (Arduino or Raspberry pi server)
- "Beerbong robot" controls. (obsolete, only required on an electornics course at university) (Arduino server)

![](https://i.imgur.com/Q0IDdyd.jpg) ![](https://i.imgur.com/2NxYEFG.jpg)