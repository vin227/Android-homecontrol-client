package com.vin227.testapp2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class IOTViewAdapter extends RecyclerView.Adapter<IOTViewAdapter.IOTViewHolder>{
    private ArrayList<IOTDevice> mDevices;
    private Context mContext;

    public IOTViewAdapter(ArrayList<IOTDevice> mDevices, Context mContext) {
        this.mDevices = mDevices;
        this.mContext = mContext;
    }

    @Override
    public IOTViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Create view from layout file to be used with viewholder
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rgbdevice_layout, parent, false);
        final IOTViewHolder holder = new IOTViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("debug", "clicked thingy" + holder.getLayoutPosition());
                MainActivity context = (MainActivity)mContext;
                context.showRelevantPopups(holder.getLayoutPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(IOTViewHolder holder,final int position) {
        holder.IOTName.setText(mDevices.get(position).name +" "+ mDevices.get(position).address.getHostAddress() +" "+ String.valueOf(mDevices.get(position).port));
    }

    @Override
    public int getItemCount() {
        return mDevices.size();
    }

    public class IOTViewHolder extends RecyclerView.ViewHolder{
        ImageView iconView;
        TextView IOTName;
        RelativeLayout parentLayout;

        public IOTViewHolder(View itemView) {
            super(itemView);
            iconView = itemView.findViewById(R.id.RGBIcon);
            IOTName = itemView.findViewById(R.id.RGBItemName);
            parentLayout = itemView.findViewById(R.id.parent);
        }

    }

}
