package com.vin227.testapp2;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.Executor;

public class MainActivity extends AppCompatActivity {
    private Integer redRGBBit = 0;
    private Integer greenRGBBit = 0;
    private Integer blueRGBBit = 0;
    private IOTViewAdapter mDeviceListAdapter;
    private BroadcastHandler mBroadcastHandler;
    public ArrayList<IOTDevice> devices = new ArrayList<>();
    public Executor mainAsyncExecutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //For communicating with devices
        UDPDeviceDiscoveryListener deviceDiscoveryListener= new UDPDeviceDiscoveryListener(this);
        deviceDiscoveryListener.execute();
        mainAsyncExecutor = deviceDiscoveryListener.THREAD_POOL_EXECUTOR;

        //For Device list
        RecyclerView recyclerView = findViewById(R.id.devicesListView);
        IOTViewAdapter deviceListAdapter = new IOTViewAdapter(devices, this);
        mDeviceListAdapter = deviceListAdapter;
        recyclerView.setAdapter(deviceListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //
        mBroadcastHandler = new BroadcastHandler(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void setRGBData() {
        //if we have devices use the first one untill some kind of selection is implemented.
        if(devices.size() > 0) {
            devices.get(0).setColor(redRGBBit, greenRGBBit, blueRGBBit);
        }
    }
    public void setRGBData(IOTDevice device) {
        //if we have devices use the first one untill some kind of selection is implemented.
        device.setColor(redRGBBit, greenRGBBit, blueRGBBit);
    }

    public void addIOTItem(final IOTDevice device) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                devices.add(device);
                mDeviceListAdapter.notifyItemInserted(devices.size() - 1);
            }
        });
    }
    public void showRelevantPopups(final int Device) {
        //display popup window according to what functions device has.
        if(devices.get(Device).hasRGB) {
            showRGBSlidersForDevice(Device);
        } else if (devices.get(Device).hasAmplifier) {
            showAmpMenuForDevice(Device);
        } else if (devices.get(Device).hasLamp) {
            showLampMenuForDevice(Device);
        }
    }
    public void showRGBSlidersForDevice(final int Device) {
        Log.d("debug", "showRGBSlidersForDevice: " + devices.get(Device).address.toString());
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;

        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);

        View RGBSettings = inflater.inflate(R.layout.rgbsliders_layout,null);

        //Show popup window for rgb controls at the center of the screen.
        View parent = findViewById(android.R.id.content);
        final PopupWindow popupWindow = new PopupWindow(RGBSettings, width, height, true);
        popupWindow.showAtLocation(parent , Gravity.CENTER, 0, 0);

        //Find relevant sliders and textboxes.
        final TextView r8bitText = RGBSettings.findViewById(R.id.r8bitText);
        final SeekBar redRGBBar = RGBSettings.findViewById(R.id.r8bitBar);
        final TextView g8bitText = RGBSettings.findViewById(R.id.g8bitText);
        final SeekBar greenRGBBar = RGBSettings.findViewById(R.id.g8bitBar);
        final TextView b8bitText = RGBSettings.findViewById(R.id.b8bitText);
        final SeekBar blueRGBBar = RGBSettings.findViewById(R.id.b8bitBar);

        redRGBBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                r8bitText.setText((String.valueOf(progress)));
                redRGBBit = progress;
                setRGBData(devices.get(Device));
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        greenRGBBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                g8bitText.setText((String.valueOf(progress)));
                greenRGBBit = progress;
                setRGBData(devices.get(Device));
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        blueRGBBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                b8bitText.setText((String.valueOf(progress)));
                blueRGBBit = progress;
                setRGBData(devices.get(Device));
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public void showAmpMenuForDevice(final int Device) {
        Log.d("debug", "showAmpMenuForDevice: " + devices.get(Device).address.toString());
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;

        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);

        View AmpSettings = inflater.inflate(R.layout.iotamp_layout,null);

        //Show popup window for rgb controls at the center of the screen.
        View parent = findViewById(android.R.id.content);
        final PopupWindow popupWindow = new PopupWindow(AmpSettings, width, height, true);
        popupWindow.showAtLocation(parent , Gravity.CENTER, 0, 0);

        //Find relevant sliders and textboxes.
        final Button powerOnButton = AmpSettings.findViewById(R.id.poweronbutton);
        final Button powerOffButton = AmpSettings.findViewById(R.id.poweroffbutton);
        final Button cdModeButton = AmpSettings.findViewById(R.id.cdbutton);
        final Button tunerModeButton = AmpSettings.findViewById(R.id.tunerbutton);

        powerOffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                devices.get(Device).toggleAmpPower(false);
            }
        });
        powerOnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                devices.get(Device).toggleAmpPower(true);
            }
        });
        cdModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                devices.get(Device).setTunerMode(IOTDevice.Mode.CD);
            }
        });
        tunerModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                devices.get(Device).setTunerMode(IOTDevice.Mode.TUNER);
            }
        });
    }
    public void showLampMenuForDevice(final int Device) {
        Log.d("debug", "showLampMenuForDevice: " + devices.get(Device).address.toString());
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;

        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);

        View LampSettings = inflater.inflate(R.layout.lampcontroller_layout,null);

        //Show popup window for rgb controls at the center of the screen.
        View parent = findViewById(android.R.id.content);
        final PopupWindow popupWindow = new PopupWindow(LampSettings, width, height, true);
        popupWindow.showAtLocation(parent , Gravity.CENTER, 0, 0);

        //Find relevant sliders and textboxes.
        final Button powerOnButton = LampSettings.findViewById(R.id.poweronbutton);
        final Button powerOffButton = LampSettings.findViewById(R.id.poweroffbutton);

        powerOffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                devices.get(Device).toggleLamp(false);
            }
        });
        powerOnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                devices.get(Device).toggleLamp(true);
            }
        });
    }

    public void sendDiscoveryBroadcast(View view){
        mBroadcastHandler.sendDiscoveryBroadcast();
    }

    public void addFakeDevices(View view) {
        //Adding fake device to list for debugging purposes
        IOTDevice first = new IOTDevice(mainAsyncExecutor);
        first.setName("n device, n:" + (devices.size()+1));
        try {
            first.address = InetAddress.getByName("0.0.0.0");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        first.hasRGB = true;
        first.port = 8888;
        addIOTItem(first);

        IOTDevice second = new IOTDevice(mainAsyncExecutor);
        second.setName("n device, n:" + (devices.size()+1));
        try {
            second.address = InetAddress.getByName("0.0.0.0");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        second.hasAmplifier = true;
        second.port = 8888;
        addIOTItem(second);

    }


}