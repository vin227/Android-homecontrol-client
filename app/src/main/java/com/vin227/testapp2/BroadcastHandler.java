package com.vin227.testapp2;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.view.View;

import java.io.IOException;
import java.net.InetAddress;

public class BroadcastHandler {
    private MainActivity mContext;
    BroadcastHandler(MainActivity initialContext){
        this.mContext = initialContext;
    }
    public void sendDiscoveryBroadcast() {
        String serverIP = "0.0.0.0";
        String serverPort = String.valueOf(8888);
        String message = "is arduino yes";
        try {
            serverIP = getBroadcastAddress().getHostAddress();
        } catch (Exception e) {
            Log.e("serverip","error",  e);
        }
        Log.d("Sendbroadcast", serverIP);
        new UDPSendActivity().executeOnExecutor(mContext.mainAsyncExecutor, serverIP, serverPort, message);
    }


    private InetAddress getBroadcastAddress() throws IOException {
        WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcp = wifi.getDhcpInfo();
        // handle null somehow

        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);
    }
}
