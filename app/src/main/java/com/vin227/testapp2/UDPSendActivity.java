package com.vin227.testapp2;


import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;

import android.widget.TextView;

public class UDPSendActivity extends AsyncTask<String, Integer, Boolean> {

    @Override
    protected Boolean doInBackground(String... arg0) {
        String messageStr = arg0[2];
        int server_port = Integer.parseInt(arg0[1]);
        byte[] message = messageStr.getBytes();
        int msg_length=message.length;

        DatagramSocket s = null;
        InetAddress local = null;

        try {
            s = new DatagramSocket(server_port);
            local = InetAddress.getByName(arg0[0]);
        }
        catch (Exception e)
        {
            Log.e("UDPSend","hmm",  e);
        }

        Log.d("Sendbroadcast", local + " " + server_port);
        try{
            final DatagramPacket p = new DatagramPacket(message, msg_length,local,server_port);
            s.send(p);
            s.close();
            Log.d("UDPSend", messageStr);
        }
        catch (Exception e)
        {
            Log.e("UDPSend","REE",  e);
        }
        return null;
    }
}