package com.vin227.testapp2;

import android.util.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Executor;

public class IOTDevice {
    Executor mainTaskAsyncExecutor;
    public String name;
    public InetAddress address;
    public Integer port;
    public boolean hasRGB = false;
    public boolean hasLamp = false;
    public boolean hasAmplifier = false;
    public enum Mode {
        CD, TUNER
    }

    //public String negociateString;

    public IOTDevice(Executor mainTaskINetExecutor) {
        //constructor for empty thingys.
        mainTaskAsyncExecutor = mainTaskINetExecutor;
        try {
            address = InetAddress.getByName("0.0.0.0");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        port = 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor (Integer r, Integer g, Integer b){

        if(address != null) {
            String RGB = formatRGBValue(r) + "," + formatRGBValue(g) + "," + formatRGBValue(b);

            Log.d("UDPSend", "sending to: " + address.toString() + ":" +String.valueOf(port));
            new UDPSendActivity().executeOnExecutor(mainTaskAsyncExecutor, address.getHostAddress(), String.valueOf(port), RGB);
        }
    }

    public void setTunerMode(Mode mode) {
        if(address != null && hasAmplifier) {
            Log.d("UDPSend", "sending to: " + address.toString() + ":" +String.valueOf(port));
            switch (mode) {
                case CD:
                    new UDPSendActivity().executeOnExecutor(mainTaskAsyncExecutor, address.getHostAddress(), String.valueOf(port), "cd");
                    break;
                case TUNER:
                    new UDPSendActivity().executeOnExecutor(mainTaskAsyncExecutor, address.getHostAddress(), String.valueOf(port), "tuner");
                    break;
            }
        }
    }
    public void toggleAmpPower(boolean powerOn) {
        if(address != null && hasAmplifier) {
            Log.d("UDPSend", "sending to: " + address.toString() + ":" +String.valueOf(port));
            if(powerOn){
                new UDPSendActivity().executeOnExecutor(mainTaskAsyncExecutor, address.getHostAddress(), String.valueOf(port), "poweron");

            } else {
                new UDPSendActivity().executeOnExecutor(mainTaskAsyncExecutor, address.getHostAddress(), String.valueOf(port), "poweroff");
            }
        }
    }
    public void toggleLamp(boolean powerOn) {
        Log.d("UDPSend", "ToggleLamp called");
        if(address != null && hasLamp) {
            Log.d("UDPSend", "sending to: " + address.toString() + ":" +String.valueOf(port));
            if(powerOn){
                new UDPSendActivity().executeOnExecutor(mainTaskAsyncExecutor, address.getHostAddress(), String.valueOf(port), "lightsOn");
            } else {
                new UDPSendActivity().executeOnExecutor(mainTaskAsyncExecutor, address.getHostAddress(), String.valueOf(port), "lightsOff");
            }
        }
    }

    public String formatRGBValue(int value) {

        if (value > 100) {
            return String.valueOf(value);
        } else if (value > 10) {
            return "0" + String.valueOf(value);
        } else {
            return "00" + String.valueOf(value);
        }
    }

    public void sendDebugPacket() {
        String message = "data xd";
        new UDPSendActivity().executeOnExecutor(mainTaskAsyncExecutor, address.getHostAddress(), String.valueOf(port), message);
        //new UDPSendActivity().execute(serverIP, serverPort, message);
    }
}
