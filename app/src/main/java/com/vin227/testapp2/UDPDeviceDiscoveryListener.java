package com.vin227.testapp2;

import android.os.AsyncTask;
import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPDeviceDiscoveryListener extends AsyncTask<String, Integer, Boolean> {
    private InetAddress latestResult;
    private MainActivity context;
    UDPDeviceDiscoveryListener(MainActivity initialContext) {
        this.context = initialContext;
    }
    @Override
    protected Boolean doInBackground(String... arg0) {
        int port = 8899;
        byte[] recvBuf = new byte[15000];
        DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);

        Log.d("UDPReceive", "Waiting for UDP broadcast");

        String senderIP;
        String message;
        try{
            final DatagramSocket clientSocket = new DatagramSocket(port);
            while (true) {
                clientSocket.receive(packet);
                senderIP = packet.getAddress().getHostAddress();
                message = new String(packet.getData(), packet.getOffset(), packet.getLength());
                Log.d("UDPReceive", "Got UDP packet from " + senderIP + ", message: " + message);
                latestResult = packet.getAddress();
                //check every device in list if already added..
                boolean alreadyAdded = false;
                for(int i = 0; i < context.devices.size(); i++) {
                    if(context.devices.get(i).address.equals(latestResult)){
                        alreadyAdded = true;
                    }
                }
                //Dont add duplicates for devices
                if(!alreadyAdded) {
                    IOTDevice foundOne = new IOTDevice(context.mainAsyncExecutor);
                    //TODO: make more modular
                    if(message.equals("amrgb")) {
                        foundOne.setName("n rdb device, n:" + (context.devices.size() + 1));
                        foundOne.hasRGB = true;
                    } else if(message.equals("amled")) {
                        foundOne.setName("Olohuoneen LEDit: " + (context.devices.size()+1));
                        foundOne.hasLamp = true;
                    } else if(message.equals("amamp")) {
                        foundOne.setName("Olohuoneen vahvistin: " + (context.devices.size()+1));
                        foundOne.hasAmplifier = true;
                    }
                    foundOne.address = latestResult;
                    foundOne.port = 8888;
                    context.addIOTItem(foundOne);
                }
            }
        }
        catch (Exception e)
        {
            Log.e("UDPReceive","REE",  e);
        }
        return null;

    }
}
